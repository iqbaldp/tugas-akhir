#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Jan 20 19:27:44 2018

@author: jmkovachi
"""
import re
import math
import nltk
import pickle
import random
import time
from nltk import tokenize
from pandas import DataFrame
from nltk.stem import WordNetLemmatizer
from sklearn.metrics import accuracy_score
from read_MPQA import extract_MPQA as MPQA
from sklearn.metrics import precision_recall_fscore_support as score
from read_movie_reviews import read_Movies as movie_reader
from collections import Counter
from stopword_list import stopword_list

class MN_NaiveBayes:

    """
    Constructor for MN_NaiveBayes.
    Initializes overall counts of positive, negative, and neutral classes.
    Initializes overall document count for use in a priori class probability
    calculation.
    Initializes pos, neg, and neutral feature count dictionaries.
    """


    def __init__(self, anak, remaja, dewasa):
        self.anak_count = MPQA.count(anak)
        self.remaja_count = MPQA.count(remaja)
        self.dewasa_count = MPQA.count(dewasa)

        self.doc_count = self.anak_count + self.remaja_count + self.dewasa_count # + self.neu_count
        self.tie_count = 0
        self.anak = anak
        self.remaja = remaja
        self.dewasa = dewasa

    """
    An implementation of Jurafsky's MN Bayes Network
    algorithm.
    """

    def idf(self, docs):
        self.idf           = {}
        self.idf['anak']   = {}
        self.idf['remaja'] = {}
        self.idf['dewasa'] = {}
        self.idf['sum']    = 0

        total_word_in_document  = 0
        for word, count in self.anak.items():
            for document in docs:
                if word in document:
                    total_word_in_document += 1
            
            value = (len(docs) + 1) / (total_word_in_document + 1)
            self.idf['anak'][word] = math.log(value)
            self.idf['sum']        += self.idf['anak'][word]
            total_word_in_document = 0


        total_word_in_document  = 0
        for word, count in self.remaja.items():
            for document in docs:
                if word in document:
                    total_word_in_document += 1
            
            value = (len(docs) + 1) / (total_word_in_document + 1)
            self.idf['remaja'][word] = math.log(value)
            self.idf['sum']          += self.idf['remaja'][word]
            total_word_in_document = 0


        total_word_in_document  = 0
        for word, count in self.dewasa.items():
            for document in docs:
                if word in document:
                    total_word_in_document += 1
            
            value = (len(docs) + 1) / (total_word_in_document + 1)
            self.idf['dewasa'][word] = math.log(value)
            self.idf['sum']          += self.idf['dewasa'][word]
            total_word_in_document = 0


    def tfidf(self, docs):
        self.tfidf              = {}
        self.tfidf['anak']      = {}
        self.tfidf['remaja']    = {}
        self.tfidf['dewasa']    = {}
        self.tfidf['sumAnak']   = 0
        self.tfidf['sumRemaja'] = 0
        self.tfidf['sumDewasa'] = 0

        for word, count in self.anak.items():
            self.tfidf['anak'][word] = self.idf['anak'][word]*count
            self.tfidf['sumAnak']    += self.tfidf['anak'][word]

        for word, count in self.remaja.items():
            self.tfidf['remaja'][word] = self.idf['remaja'][word]*count
            self.tfidf['sumRemaja']    += self.tfidf['remaja'][word]

        for word, count in self.dewasa.items():
            self.tfidf['dewasa'][word] = self.idf['dewasa'][word]*count
            self.tfidf['sumDewasa']    += self.tfidf['dewasa'][word]


    def train(self):
        token_anak   = DataFrame (columns=['Token', 'Frekuensi', 'Bobot'])
        token_remaja = DataFrame (columns=['Token', 'Frekuensi', 'Bobot'])
        token_dewasa = DataFrame (columns=['Token', 'Frekuensi', 'Bobot'])

        self.features                   = {}
        self.features['anakFeatures']   = {}
        self.features['remajaFeatures'] = {}
        self.features['dewasaFeatures'] = {}
        
        # Gathering a priori probabilities by class
        # self.priorLogAnak = math.log(self.anak_count/self.doc_count)
        # self.priorLogRemaja = math.log(self.remaja_count/self.doc_count)
        # self.priorLogDewasa = math.log(self.dewasa_count/self.doc_count)
        self.priorLogAnak   = 1/3
        self.priorLogRemaja = 1/3
        self.priorLogDewasa = 1/3

        """
        Each for loop below is calculating probabilities of each feature
        for each class.
        Backslashes in calculations are added for readiblity and serve as 
        line breaks.
        """

        for word, count in self.anak.items():
            self.features['anakFeatures'][word] = math.log((self.tfidf['anak'][word] + 1) \
                                            /(self.tfidf['sumAnak'] + self.idf['sum']))
            token_anak = token_anak.append({'Token': word, 'Frekuensi': count, 'Bobot': self.features['anakFeatures'][word]}, ignore_index=True)
        
        for word, count in self.remaja.items():
            self.features['remajaFeatures'][word] = math.log((self.tfidf['remaja'][word] + 1) \
                                            /(self.tfidf['sumRemaja'] + self.idf['sum']))
            token_remaja = token_remaja.append({'Token': word, 'Frekuensi': count, 'Bobot': self.features['remajaFeatures'][word]}, ignore_index=True)
        
        for word, count in self.dewasa.items():
            self.features['dewasaFeatures'][word] = math.log((self.tfidf['dewasa'][word] + 1) \
                                            /(self.tfidf['sumDewasa'] + self.idf['sum']))
            token_dewasa = token_dewasa.append({'Token': word, 'Frekuensi': count, 'Bobot': self.features['dewasaFeatures'][word]}, ignore_index=True)


        self.features['smoothAnakFeatures']   = math.log((0 + 1) / (self.tfidf['sumAnak'] + self.idf['sum']))
        self.features['smoothRemajaFeatures'] = math.log((0 + 1) / (self.tfidf['sumRemaja'] + self.idf['sum']))
        self.features['smoothDewasaFeatures'] = math.log((0 + 1) / (self.tfidf['sumDewasa'] + self.idf['sum']))

        # Save Model Train
        with open('model.data', 'wb') as filehandle:
            pickle.dump(self.features, filehandle)

        token_anak   = token_anak.sort_values(by=['Frekuensi'], ascending=False)
        token_remaja = token_remaja.sort_values(by=['Frekuensi'], ascending=False)
        token_dewasa = token_dewasa.sort_values(by=['Frekuensi'], ascending=False)

        token_anak.to_html('token_anak.html')
        token_remaja.to_html('token_remaja.html')
        token_dewasa.to_html('token_dewasa.html')

    """
    Takes a given test document and make a classification decision based off
    of a max probability.
    @param document: Test document used to make classification decision.
    return: A two-tuple with the classification decision and its corresponding
    log-space probability.
    """
    def test(self, document, POS=False):
        # features = []

        # Load Model Train
        # with open('model.data', 'rb') as filehandle:
        #     features = pickle.load(filehandle)
        
        document = document.lower()
        document = re.sub(r'([^a-zA-Z ]+?)', ' ', document)

        for stop in stopword_list:
                if stop in document:
                    document = document.replace(stop, '')

        document = nltk.word_tokenize(document)

        # if POS:
        #     document = nltk.pos_tag(document)
        #     document = [str(word[0] + '-' + word[1]) for word in document]
        
        anak_val = math.log(self.priorLogAnak)
        remaja_val = math.log(self.priorLogRemaja)
        dewasa_val = math.log(self.priorLogDewasa)
        
        # Smoothed probabilities are calculated below, these are used when a 
        # word in the test document is not found in the given class but is found
        # in another class's feature dict

        smooth_anak   = math.log((0 + 1) / (self.tfidf['sumAnak'] + self.idf['sum']))
        smooth_remaja = math.log((0 + 1) / (self.tfidf['sumRemaja'] + self.idf['sum']))
        smooth_dewasa = math.log((0 + 1) / (self.tfidf['sumDewasa'] + self.idf['sum']))

        for feature in self.features:
            if feature == 'anakFeatures':
                for word in document:
                    if word in self.features['anakFeatures']:
                        anak_val += self.features['anakFeatures'][word]
                    else:
                        anak_val += smooth_anak
                    # print(anak)
            elif feature == 'remajaFeatures':
                for word in document:
                    if word in self.features['remajaFeatures']:
                        remaja_val += self.features['remajaFeatures'][word]
                    else:
                        remaja_val += smooth_remaja
            elif feature == 'dewasaFeatures':
                for word in document:
                    if word in self.features['dewasaFeatures']:
                        dewasa_val += self.features['dewasaFeatures'][word]
                    else:
                        dewasa_val += smooth_dewasa


        # print(anak_val, "\n")
        # print(remaja_val, "\n")
        # print(dewasa_val, "\n")

        if anak_val > remaja_val and anak_val > dewasa_val:
            # print('anak', anak_val)
            return ('anak', anak_val)
        elif remaja_val > anak_val and remaja_val > dewasa_val:
            # print('remaja', remaja_val)
            return ('remaja', remaja_val)
        elif dewasa_val > anak_val and dewasa_val > remaja_val:
            # print('dewasa', dewasa_val)
            return ('dewasa', dewasa_val)
        else:
            # self.tie_count += 1
            print("gatauw", anak_val, " ", remaja_val, " ", dewasa_val)
            return ('gatauw', anak_val)

    @staticmethod
    def classify(document, POS=False):
        features = []

        # Load Model Train
        with open('model.data', 'rb') as filehandle:
            features = pickle.load(filehandle)
        
        document = document.lower()
        document = re.sub(r'([^a-zA-Z ]+?)', ' ', document)

        for stop in stopword_list:
                if stop in document:
                    document = document.replace(stop, '')

        document = nltk.word_tokenize(document)
        
        anak_val   = math.log(1/3)
        remaja_val = math.log(1/3)
        dewasa_val = math.log(1/3)

        for feature in features:
            if feature == 'anakFeatures':
                for word in document:
                    if word in features['anakFeatures']:
                        anak_val += features['anakFeatures'][word]
                    else:
                        anak_val += features['smoothAnakFeatures']
                    # print(anak)
            elif feature == 'remajaFeatures':
                for word in document:
                    if word in features['remajaFeatures']:
                        remaja_val += features['remajaFeatures'][word]
                    else:
                        remaja_val += features['smoothRemajaFeatures']
            elif feature == 'dewasaFeatures':
                for word in document:
                    if word in features['dewasaFeatures']:
                        dewasa_val += features['dewasaFeatures'][word]
                    else:
                        dewasa_val += features['smoothDewasaFeatures']

        if anak_val > remaja_val and anak_val > dewasa_val:
            # print('anak', anak_val)
            return ('anak', anak_val)
        elif remaja_val > anak_val and remaja_val > dewasa_val:
            # print('remaja', remaja_val)
            return ('remaja', remaja_val)
        elif dewasa_val > anak_val and dewasa_val > remaja_val:
            # print('dewasa', dewasa_val)
            return ('dewasa', dewasa_val)
        else:
            # self.tie_count += 1
            print("gatauw", anak_val, " ", remaja_val, " ", dewasa_val)
            return ('gatauw', anak_val)
        
        
    """
    Downloads an article from NYT to use for testing.
    """
    @staticmethod
    def eval(obj, text_anak_test, text_remaja_test, text_dewasa_test):             
        annotated_array = []
        predicted_array = []

        for text in text_anak_test:
            outcome = obj.test(text, False)
            annotated_array.append(0)
            if outcome[0] == 'anak':
                predicted_array.append(0)
            elif outcome[0] == 'remaja':
                predicted_array.append(1)
            elif outcome[0] == 'dewasa':
                predicted_array.append(3)
        
        for text in text_remaja_test:
            outcome = obj.test(text, False)
            annotated_array.append(1)
            if outcome[0] == 'anak':
                predicted_array.append(0)
            elif outcome[0] == 'remaja':
                predicted_array.append(1)
            elif outcome[0] == 'dewasa':
                predicted_array.append(3)

        for text in text_dewasa_test:
            outcome = obj.test(text, False)
            annotated_array.append(3)
            if outcome[0] == 'anak':
                predicted_array.append(0)
            elif outcome[0] == 'remaja':
                predicted_array.append(1)
            elif outcome[0] == 'dewasa':
                predicted_array.append(3)
                
        precision, recall, fscore, support = score(annotated_array, predicted_array)

        # print(annotated_array)
        # print(predicted_array)
        
        print('precision: {}'.format(precision))
        print('recall: {}'.format(recall))
        # print('fscore: {}'.format(fscore))
        # print('support: {}'.format(support))   
        print('accuracy: {}'.format(accuracy_score(annotated_array, predicted_array)))
        print("---")

        return (sum(precision)/len(precision)), (sum(recall)/len(recall)), accuracy_score(annotated_array, predicted_array)


    def cross_validation(self, text_anak, text_remaja, text_dewasa):
        
        for text in text_anak:
            print(text)    

    @staticmethod
    def get_train_test_data(text_anak, text_remaja, text_dewasa, count):
        # print(len(text_anak))
        text_anak_train   = []
        text_remaja_train = []
        text_dewasa_train = []

        text_anak_test    = []
        text_remaja_test  = []
        text_dewasa_test  = []

        range_test        = []
        range_train       = []

        x = 0
        for x in range(count+10):
            if x >= count:
                range_test.append(x)

        # get test&train anak
        # # random.shuffle(text_anak)
        y = 0
        for text in text_anak:
            if y in range_test:
                text_anak_test.append(text)
            else:
                # print(text[:10])
                range_train.append(y)
                text_anak_train.append(text)
            y+=1    

        # # get test&train remaja
        # # random.shuffle(text_remaja)
        y = 0
        for text in text_remaja:
            if y in range_test:
                text_remaja_test.append(text) if text else None
            else:
                # print(text[:10])
                range_train.append(y)
                text_remaja_train.append(text)
            y+=1   

        # get test&train dewasa
        # # random.shuffle(text_dewasa)
        y = 0
        for text in text_dewasa:
            if y in range_test:
                text_dewasa_test.append(text)
            else:
                # print(text[:10])
                range_train.append(y)
                text_dewasa_train.append(text)
            y+=1   


        return text_anak_train, text_remaja_train, text_dewasa_train, text_anak_test, text_remaja_test, text_dewasa_test


    @staticmethod
    def assign(sentences, sent_array, bayes_object):
        count = 0
        annotated_array = []
        predicted_array = []
        for sentence in sentences:
            
            outcome = bayes_object.test(sentence)
            
            if sent_array[count] == 'X':
                count += 1
                continue
            elif sent_array[count] == 'P':
                annotated_array.append(0)
            elif sent_array[count] == 'N':
                annotated_array.append(1)
            else:
                print('Malformed input for sentence: ' + sentences[count])
                count += 1
                continue
            
            if outcome[0] == 'positive':
                predicted_array.append(0)
            elif outcome[0] == 'negative':
                predicted_array.append(1)
 
            count += 1
            
        precision, recall, fscore, support = score(annotated_array, predicted_array)
        
        
        print('precision: {}'.format(precision))
        print('recall: {}'.format(recall))
        print('fscore: {}'.format(fscore))
        print('support: {}'.format(support))
        
    @staticmethod    
    def count_POS(words):
        cnt_anak = Counter()
        cnt_remaja = Counter()
        cnt_dewasa = Counter()

        anak   = [str(word[0]) for word in words if word[1] == 'anak']
        
        remaja = [str(word[0]) for word in words if word[1] == 'remaja']

        dewasa = [str(word[0]) for word in words if word[1] == 'dewasa']
        
        for word in anak:
            cnt_anak[word] += 1
            
        for word in remaja:
            cnt_remaja[word] += 1

        for word in dewasa:
            cnt_dewasa[word] += 1
            
        return cnt_anak, cnt_remaja, cnt_dewasa
        
    
        
                  
# mpqa = MPQA()
start_time = time.time()
df = DataFrame (columns=['Precision', 'Recall', 'Accuracy'])
text_anak, text_remaja, text_dewasa, docs = movie_reader.tag_dir('/home/iqbaldp/Documents/Kuliah/pos_tag_ind/VB/')

for x in range(30):
    count = 0
    accuracy  = 0
    precision = 0
    recall    = 0
    for loop in range(10):

        text_anak_train, text_remaja_train, text_dewasa_train, text_anak_test, text_remaja_test, text_dewasa_test = MN_NaiveBayes.get_train_test_data(text_anak, text_remaja, text_dewasa, count)

        # print(len(text_anak_train), " - ", len(text_anak_test))
        # print(len(text_remaja_train), " - ", len(text_remaja_test))
        # print(len(text_dewasa_train), " - ", len(text_dewasa_test))
        # print("============== {} =========")

        # if count == 20:
        #     print(text_dewasa_test)

        count += 10
        # print("Train anak ", len(text_anak_train))
        # print("Train remaja ", len(text_remaja_train))
        # print("Train dewasa ", len(text_dewasa_train))

        # print("Test anak ", len(text_anak_test))
        # print("Test remaja ", len(text_remaja_test))
        # print("Test dewasa ", len(text_dewasa_test))

        words = movie_reader.tokenizing(text_anak_train, text_remaja_train, text_dewasa_train)

        # words = [x for x in words if x not in stopword_list]
        
        anak, remaja, dewasa = MN_NaiveBayes.count_POS(words)

        NB = MN_NaiveBayes(anak, remaja, dewasa)

        train_data = text_anak_train + text_remaja_train + text_dewasa_train

        NB.idf(train_data)
        NB.tfidf(train_data)
        NB.train()
        # document = "Pernahkah teman-teman menemukan kucing di bawah kolong meja rumah makan atau restoran? Yap, kadang-kadang ada kucing yang mengeong di bawah kolong meja, seolah ingin meminta makanan kita. Karena kasihan, kebanyakan orang akan memberi sedikit makanannya ke kucing tadi. Apakah teman-teman pernah memberi makan kucing yang ada di kolong meja atau bawah meja? Pada dasarnya kebiasaan ini memang baik, teman-teman. Kita jadi bisa membantu kucing terlantar untuk mendapatkan makanan sehingga mereka tidak kelaparan. Baca Juga: Baru Pertama Kali Pelihara Kucing? Begini Cara Memberi Makan Kucing Namun, ternyata kita disarankan untuk tidak melakukannya, lo. Selain dapat mengganggu pelanggan lain, hal ini juga bisa membahayakan kucing itu sendiri. Mengapa begitu? Yuk, cari tahu alasannya!"
        # NB.test(document, False)
        a, b, c = NB.eval(NB, text_anak_test, text_remaja_test, text_dewasa_test)
        precision += a
        recall    += b
        accuracy  += c

    df = df.append({'Precision': (precision/10), 'Recall': (recall/10), 'Accuracy': (accuracy/10)}, ignore_index=True)
    random.shuffle(text_anak)
    random.shuffle(text_remaja)
    random.shuffle(text_dewasa)

    print("--------------------------------------------------------")
    df.to_html('VB_STEM.html')
    

# document = "Pernahkah teman-teman menemukan kucing di bawah kolong meja rumah makan atau restoran? Yap, kadang-kadang ada kucing yang mengeong di bawah kolong meja, seolah ingin meminta makanan kita. Karena kasihan, kebanyakan orang akan memberi sedikit makanannya ke kucing tadi. Apakah teman-teman pernah memberi makan kucing yang ada di kolong meja atau bawah meja? Pada dasarnya kebiasaan ini memang baik, teman-teman. Kita jadi bisa membantu kucing terlantar untuk mendapatkan makanan sehingga mereka tidak kelaparan. Baca Juga: Baru Pertama Kali Pelihara Kucing? Begini Cara Memberi Makan Kucing Namun, ternyata kita disarankan untuk tidak melakukannya, lo. Selain dapat mengganggu pelanggan lain, hal ini juga bisa membahayakan kucing itu sendiri. Mengapa begitu? Yuk, cari tahu alasannya!"
# MN_NaiveBayes.classify(document, False)

print("--- %s seconds ---" % (time.time() - start_time))