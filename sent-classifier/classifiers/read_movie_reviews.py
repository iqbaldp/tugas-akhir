#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Jan 28 15:00:01 2018

@author: jmkovachi
"""

import os
import re
import nltk
from nltk.corpus import stopwords
from nltk.probability import FreqDist
from stopword_list import stopword_list
from Sastrawi.Stemmer.StemmerFactory import StemmerFactory

class read_Movies:

    @staticmethod
    def read_dir(base_dir):
        feature_list = []
        
        doc_list = []
        
        stops = set(stopwords.words('english'))
        print(stops)
        for pos in os.listdir(base_dir + 'pos'):
            file = open(base_dir + 'pos/' + pos)
            text = file.read()
            for sentence in nltk.sent_tokenize(text):
                split_sentence = [word for word in nltk.word_tokenize(sentence) if word not in stops]
                for word in split_sentence:

                    if "\n" in word:
                        word = word.replace('\n','')
                    elif word == '' or word == '!' or word == ';' or word == '?' or word == ';':
                        continue
                    feature_list.append(word)
                    
                
                doc_list.append([set(split_sentence), 'positive', {'positive' : 1, 'negative' : 1}])
                
        for neg in os.listdir(base_dir + 'neg'):
            file = open(base_dir + 'neg/' + neg)
            text = file.read()
            for sentence in nltk.sent_tokenize(text):
                split_sentence = [word for word in nltk.word_tokenize(sentence) if word not in stops]
                for word in split_sentence:
                    if "\n" in word:
                        word = word.replace('\n','')
                    elif word == '' or word == '!' or word == ';' or word == '?' or word == ';':
                        continue 
                    feature_list.append(word)
                doc_list.append([set(sentence.split(' ')), 'negative', {'positive' : 1, 'negative' : 1}])
        
        feature_list = FreqDist(feature_list)
        
        new_list = []
        for word, frequency in feature_list.most_common(5000):
            new_list.append(word)
            
        return list(set(new_list)), doc_list
    
    def tag_dir(base_dir):
        factory = StemmerFactory()
        stemmer = factory.create_stemmer()
        
        text_anak       = []
        text_remaja     = []
        text_dewasa     = []
        docs            = []
        pos_index_count = 0
        test_docs_pos   = []
        
        for pos in os.listdir(base_dir + 'anak'):
            # file = open(base_dir + 'pos/' + pos)
            # text = file.read()
            with open(base_dir + 'anak/' + pos, 'rb') as f:
                text = f.read().decode(errors='replace')
            # if pos_index_count / len(os.listdir(base_dir + 'anak')) > .75:
            #     test_docs_pos.append(text)
            #     continue

            text = text.lower()
            text = re.sub(r'([^a-zA-Z ]+?)', ' ', text)

            words = text.split()
            words = [x for x in words if x not in stopword_list]

            text = " ".join(words)
            text = stemmer.stem(text)
            # stopwordlist
            # for stop in stopword_list:
            #     if stop in text:
            #         text = text.replace(stop, '') 

            docs.append(text)
            text_anak.append(text)
            pos_index_count += 1
            # for word in nltk.word_tokenize(text):
            #     if "\n" in word:
            #         word = word.replace('\n','')
            #     elif word == '' or word == '!' or word == ';' or word == '?' or word == ';':
            #         continue

            #     words.append((word, 'anak'))    

               
        print('Read Anak Docs Finish')
        neg_index_count = 0
        test_docs_neg = []
        for neg in os.listdir(base_dir + 'remaja'):
            with open(base_dir + 'remaja/' + neg, 'rb') as f:
                text = f.read().decode(errors='replace')

            text = text.lower()
            text = re.sub(r'([^a-zA-Z ]+?)', ' ', text)

            words = text.split()
            words = [x for x in words if x not in stopword_list]
                        
            text = " ".join(words)
            text = stemmer.stem(text)
            # stopwordlist
            # for stop in stopword_list:
            #     if stop in text:
            #         text = text.replace(stop, '') 

            docs.append(text)
            text_remaja.append(text)
            # if neg_index_count / len(os.listdir(base_dir + 'remaja')) > .75:
            #     test_docs_neg.append(text)
            #     continue
            neg_index_count += 1
            # for word in nltk.word_tokenize(text):
            #     if "\n" in word:
            #         word = word.replace('\n','')
            #     elif word == '' or word == '!' or word == ';' or word == '?' or word == ';':
            #         continue

            #     words.append((word, 'remaja'))


        print('Read Remaja Docs Finish')
        neg_index_count = 0
        test_docs_neg = []
        for neg in os.listdir(base_dir + 'dewasa'):
            with open(base_dir + 'dewasa/' + neg, 'rb') as f:
                text = f.read().decode(errors='replace')

            text = text.lower()
            text = re.sub(r'([^a-zA-Z ]+?)', ' ', text)

            words = text.split()
            words = [x for x in words if x not in stopword_list]
                        
            text = " ".join(words)
            text = stemmer.stem(text)
            # stopwordlist
            # for stop in stopword_list:
            #     if stop in text:
            #         text = text.replace(stop, '')

            docs.append(text)
            text_dewasa.append(text)
            # if neg_index_count / len(os.listdir(base_dir + 'dewasa')) > .75:
            #     test_docs_neg.append(text)
            #     continue
            neg_index_count += 1
            # for word in nltk.word_tokenize(text):
            #     if "\n" in word:
            #         word = word.replace('\n','')
            #     elif word == '' or word == '!' or word == ';' or word == '?' or word == ';':
            #         continue

            #     words.append((word, 'dewasa'))

        
        return text_anak, text_remaja, text_dewasa, docs

    def tokenizing(text_anak, text_remaja, text_dewasa):
        words  = []

        # tokenizing anak
        for text in text_anak:
            # for word in nltk.word_tokenize(text):
            for word in text.split():
                if "\n" in word:
                    word = word.replace('\n','')
                elif word == '' or word == '!' or word == ';' or word == '?' or word == ';':
                    continue

                # for stop in stopword_list:
                #     if stop != word:
                words.append((word, 'anak'))

        # tokenizing remaja
        for text in text_remaja:
            # for word in nltk.word_tokenize(text):
            for word in text.split():
                if "\n" in word:
                    word = word.replace('\n','')
                elif word == '' or word == '!' or word == ';' or word == '?' or word == ';':
                    continue

                # for stop in stopword_list:
                #     if stop != word:
                words.append((word, 'remaja'))

        # tokenizing dewasa
        for text in text_dewasa:
            # for word in nltk.word_tokenize(text):
            for word in text.split():
                if "\n" in word:
                    word = word.replace('\n','')
                elif word == '' or word == '!' or word == ';' or word == '?' or word == ';':
                    continue

                # for stop in stopword_list:
                #     if stop != word:
                words.append((word, 'dewasa'))

        return words


    @staticmethod
    def read_for_bayes(base_dir):
        pos_list = []
        neg_list = []
        
        pos_index_count = 0
        test_docs_pos = []
        for pos in os.listdir(base_dir + 'pos'):
            file = open(base_dir + 'pos/' + pos)
            text = file.read()
            if pos_index_count / len(os.listdir(base_dir + 'pos')) > .75:
                test_docs_pos.append(text)
                continue
            pos_index_count += 1
            for sentence in text.split('.'):
                stops = set(stopwords.words('english'))
                split_sentence = [word for word in sentence.split(' ') if word not in stops]
                for word in split_sentence:
                    pos_list.append((word, 'positive'))
            
    
        neg_index_count = 0
        test_docs_neg = []
        for neg in os.listdir(base_dir + 'neg'):
            file = open(base_dir + 'neg/' + neg)
            text = file.read()
            if neg_index_count / len(os.listdir(base_dir + 'neg')) > .75:
                test_docs_neg.append(text)
                continue
            neg_index_count += 1
            for sentence in text.split('.'):
                for word in sentence.split(' '):
                    neg_list.append((word, 'negative'))
            
        
        return pos_list, neg_list, test_docs_pos, test_docs_neg
        
        