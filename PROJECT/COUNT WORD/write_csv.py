from nltk.tokenize import sent_tokenize, word_tokenize
from nltk.corpus import stopwords
from stopword_list import stopword_list
import csv
import re
import string

frequency = {}
document_text = open('BOBO.txt', 'r')
text_string = document_text.read().lower()
match_pattern = re.findall(r'\b[a-z-]{3,15}\b', text_string)


removed = []
for t in match_pattern:
    if t not in stopword_list:
        removed.append(t)

 
for word in removed:
    count = frequency.get(word,0)
    frequency[word] = count + 1
     
frequency_list = frequency.keys()

with open('BOBO.csv', mode='w') as csv_file:
    fieldnames = ['TERM', 'FREQUENCY']
    writer = csv.DictWriter(csv_file, fieldnames=fieldnames)

    writer.writeheader()
    for words in frequency_list:
	    # print (words, frequency[words])
	    writer.writerow({'TERM': words, 'FREQUENCY': frequency[words]})