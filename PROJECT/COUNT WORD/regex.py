words = ["Hey", "!", "How", "are", "you", "?", "I", "would", "like", "a", "sandwich", "."]

sentences = [[]]
ends = set(".?!")
for word in words:
    if word in ends: sentences.append([])
    else: sentences[-1].append(word)

if sentences[0]:
    if not sentences[-1]: sentences.pop()
    print("average sentence length:", sum(len(s) for s in sentences)/len(sentences))