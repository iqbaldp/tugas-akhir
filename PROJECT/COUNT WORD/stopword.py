from Sastrawi.StopWordRemover.StopWordRemoverFactory import StopWordRemoverFactory, StopWordRemover, ArrayDictionary
 
s = 'Aku pernah yang mendengar Aisya bercerita bahwa sebenarnya ia tidak terlalu senang dengan kabar perjodohan yang diatur oleh orang tuanya.'
 
# Ambil Stopword bawaan
stop_factory = StopWordRemoverFactory().get_stop_words()
del stop_factory[:]
more_stopword = ['yang', 'dan', 'dengan', 'serta', 'atau', 'tetapi', 'namun', 'sedangkan', 'sebaliknya', 'bahkan', 'melainkan', 'hanya']
 
# Merge stopword
data = stop_factory + more_stopword
 
dictionary = ArrayDictionary(data)
str = StopWordRemover(dictionary)
 
print(str.remove(s))