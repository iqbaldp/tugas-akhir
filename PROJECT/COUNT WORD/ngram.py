import nltk, re, string, collections
from nltk.util import ngrams # function for making ngrams

document_text = open('BOBO.csv', 'r')
text = document_text.read().lower()

text = re.sub('ENDOFARTICLE.','',text)

punctuationNoPeriod = "[" + re.sub("\.","",string.punctuation) + "]"
text = re.sub(punctuationNoPeriod, "", text)

tokenized = text.split()

esBigrams = ngrams(tokenized, 2)

esBigramFreq = collections.Counter(esBigrams)


for word, count in esBigramFreq.most_common(10):
	print(word)